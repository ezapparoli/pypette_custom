rule bamToBigwig:
  """
  Converts any bam file to bigwig.
  """
  input:
    bam = "{anyprefix}/{sample_name}.bam",
    bai = "{anyprefix}/{sample_name}.bai"
  output:
    bw  = "{anyprefix}/{sample_name}.bw"
  run:
    cmd = f"""
      bamCoverage      \
        -b {input.bam} \
        -o {output.bw}
    """
    exshell(**vars())

rule bamToBigwigStranded:
  """
  Converts any bam in bigwig as stranded: reverse or forward.
  """
  input:
    bam = "{anyprefix}/{sample_name}.bam",
    bai = "{anyprefix}/{sample_name}.bai"
  output:
    bw  = "{anyprefix}/{sample_name}.{strand}.bw"
  wildcard_constraints:
    strand = 'forward|reverse'
  run:
    cmd = f"""
      bamCoverage                             \
        -b {input.bam}                        \
        -o {output.bw}                        \
        --filterRNAstrand {wildcards.strand}
    """
    exshell(**vars())
 
ruleorder: bamToBigwigStranded > bamToBigwig
